#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 11:51:18 2018

@author: puszek
"""

import sys

import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import ExtraTreesClassifier
from sklearn import cross_validation
from sklearn.metrics import classification_report

from utilities import visualise_classifier

# load input data
input_file = 'data_imbalance.txt'
data = np.loadtxt(input_file, delimiter=',')
X, y = data[:, :-1], data[:, -1]

# separate data into two classes
class_0 = X[y==0]
class_1 = X[y==1]

# Visualisation via scatter plot

plt.figure()
plt.scatter(class_0[:, 0], class_0[:, 1], s=75, facecolors='black',
            edgecolors='black', linewidth=1, marker='x')
plt.scatter(class_1[:, 0], class_1[:, 1], s=75, facecolors='white',
            edgecolors='black', linewidth=1, marker='o')
plt.title('Input data')

# Split data into training and test datasets
X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.25, random_state=5)

# Extremely random forests classifier
params = { 'n_estimators': 100, 'max_depth': 4, 'random_state': 0}
if len(sys.argv) > 1:
    if sys.argv[1] == 'balance':
        params = {'n_estimators': 100, 'max_depth': 4, 'random_state': 0, 'class_weight': 'balanced'}
    else:
        raise TypeError("Invalid input argument; should be 'balance'")
        
# Build train and so on
classifier = ExtraTreesClassifier(**params)
classifier.fit(X_train, y_train)
visualise_classifier(classifier, X_train, y_train, 'Training dataset')

y_test_pred = classifier.predict(X_test)
visualise_classifier(classifier, X_test, y_test, 'Test dataset')

# Compute the performance
class_names = ['Class-0', 'Class-1']
print("\n" + "#"*40 +"\n")
print("Classifier performance on training dataset\n")
print(classification_report(y_train, classifier.predict(X_train), target_names=class_names))
print("\n" + "#"*40 +"\n")

print("\n" + "#"*40 +"\n")
print("Classification performance for test dataset\n")
print(classification_report(y_test, y_test_pred, target_names=class_names))
print("\n" + "#"*40 +"\n")
      
plt.show()