#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 13:57:11 2018

@author: puszek
"""

import numpy as np
from sklearn import preprocessing

# Defining sample input labels
input_labels = ['red', 'black', 'red', 'green', 'black', 'yellow', 'blue']

# Create label encoder and fit the labels
encoder = preprocessing.LabelEncoder()
encoder.fit(input_labels)

# Mapping
print("\nLabel mapping:")
for i, item in enumerate(encoder.classes_):
    print(item, '-->', i)

# Encode a set of labels using the encoder
test_labels = ['green', 'red', 'black']
encoded_values = encoder.transform(test_labels)
print("\nLabels = ",test_labels)
print("Encoded values = ", list(encoded_values)) 

# Decoding random set of numbers using the encoder
encoded_values = [3, 1, 0, 4, 2]
decoded_list = encoder.inverse_transform(encoded_values)
print("\nEncoded values =", encoded_values )
print("\nDecoded values =", list(decoded_list))