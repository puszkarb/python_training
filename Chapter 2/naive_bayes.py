#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 14:05:16 2018

@author: puszek
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn import cross_validation

from utilities import visualise_classifier

# Input file containing data
input_file = 'data_multivar_nb.txt'

# Loading data from input file
data = np.loadtxt(input_file, delimiter=',')
x, y = data[:, :-1], data[:, -1]

# Create classifier
classifier = GaussianNB()

# Train the classifier
classifier.fit(x,y)

# Run the classifier on training data, and predic output
y_pred = classifier.predict(x)

# Compute the acuracy
accuracy = 100.0 * (y == y_pred).sum() / x.shape[0]
print("Accuracy =", round(accuracy,2), "%")

# Visualise classifier
visualise_classifier(classifier, x, y)

#########################################
# Cross validation

# Split data int otraining and test data
x_train, x_test, y_train, y_test = cross_validation.train_test_split(x,y, test_size = 0.2, random_state = 3)
classifier_new = GaussianNB()
classifier_new.fit(x_train,y_train)
y_test_pred = classifier_new.predict(x_test)

# compute accuracy of the new classifier
accuracy = 100.0 * (y_test == y_test_pred).sum() / x_test.shape[0]
print("Accuracy of the new classifier =", round(accuracy, 2), "%")

# Visualise
visualise_classifier(classifier_new, x_test, y_test)

#######################################
# Scoring functions
num_folds = 3
accuracy_values = cross_validation.cross_val_score(classifier, x, y, scoring='accuracy', cv=num_folds)
print("Accuracy: " + str(round(100*accuracy_values.mean(), 2)) + "%")

precision_values = cross_validation.cross_val_score(classifier, x, y, scoring='precision_weighted', cv=num_folds)
print("Precision: " + str(round(100*precision_values.mean(),2)) + "%")

recall_values = cross_validation.cross_val_score(classifier, x, y, scoring='recall_weighted', cv=num_folds)
print("Recall: " + str(round(100*recall_values.mean(),2)) + "%")

f1_values = cross_validation.cross_val_score(classifier, x, y, scoring='f1_weighted', cv=num_folds)
print("F1: " + str(round(100*f1_values.mean(),2)) + "%")

