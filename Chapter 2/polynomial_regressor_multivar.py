#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 18:46:34 2018

@author: puszek
"""

import numpy as np
from sklearn import linear_model
import sklearn.metrics as sm
from sklearn.preprocessing import PolynomialFeatures

# Input file containing data
input_file = 'data_multivar_regr.txt'

# Load data from the input file
data = np.loadtxt(input_file, delimiter=',')
X,y = data[:, :-1], data[:, -1]

# Split data into training and testing data
num_training = int(0.8 * len(X))
num_test = len(X) - num_training

# Training data
X_train, y_train = X[:num_training], y[:num_training]

# Test data
X_test, y_test = X[num_training:], y[num_training:]

# Create linear regressor model
regressor = linear_model.LinearRegression()

# Train the regressor using training sets
regressor.fit(X_train,y_train)
y_test_pred = regressor.predict(X_test)

# Print out the performance
print("Linear Regressor performance:")
print("Mean absolute error=", round(sm.mean_absolute_error(y_test, y_test_pred),2))
print("Mean squared error=", round(sm.mean_squared_error(y_test, y_test_pred),2))
print("Median absolute error =", round(sm.median_absolute_error(y_test, y_test_pred),2))
print("Explained variance score =", round(sm.explained_variance_score(y_test, y_test_pred),2))
print("R2 score =", round(sm.r2_score(y_test, y_test_pred),2))

# Create polynomial regressor
polynomial_regressor = PolynomialFeatures(degree=10)
X_train_transformed = polynomial_regressor.fit_transform(X_train)
datapoint =[[7.75, 6.35, 5.56]]
poly_datapoint = polynomial_regressor.fit_transform(datapoint)

poly_linear_model = linear_model.LinearRegression()
poly_linear_model.fit(X_train_transformed, y_train)
print("\nLinear regression:\n", regressor.predict(datapoint))
print("Polynomial regression: \n", poly_linear_model.predict(poly_datapoint))


