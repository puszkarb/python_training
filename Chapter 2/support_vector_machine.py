#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 23 14:11:15 2018

@author: puszek
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsOneClassifier
from sklearn import cross_validation

# Input file containing data
input_file = 'income_data.txt'

# Read the data
X = []
y = []
count_class1 = 0
count_class2 = 0
max_datapoints = 25000

with open(input_file, 'r') as f:
    for line in f.readlines():
        if count_class1 >= max_datapoints and count_class2 >= max_datapoints:
            break
        
        if '?' in line:
            continue
        
        data = line[:-1].split(', ')
        if data[-1] == '<=50K' and count_class1 < max_datapoints:
            X.append(data)
            count_class1 +=1
        
        if data[-1] == '>50K' and count_class2 < max_datapoints:
            X.append(data)
            count_class2 +=1
            
# Convert to a numpy array
X = np.array(X)

# Convert string data to numerical data
label_encoder = []
X_encoded = np.empty(X.shape)
for i, item in enumerate(X[0]):
    if item.isdigit():
        X_encoded[:, i] = X[:, i]
    else:
        label_encoder.append(preprocessing.LabelEncoder())
        X_encoded[:, i] = label_encoder[-1].fit_transform(X[:, i])

X = X_encoded[:, :-1].astype(int)
y = X_encoded[:, -1].astype(int)

# Create SVM classifier
classifier = OneVsOneClassifier(LinearSVC(random_state=0))

# Traing the classifier
classifier.fit(X,y)

#Cross validation

X_train, X_test, y_train, y_test = cross_validation.train_test_split(X,y, test_size=0.2, random_state = 5)
classifier = OneVsOneClassifier(LinearSVC(random_state=0))
classifier.fit(X_train, y_train)
y_test_pred = classifier.predict(X_test)

# Compute F1 score of the SVM Classifier
f1 = cross_validation.cross_val_score(classifier, X, y, scoring ='f1_weighted', cv=3)
print("F1 score: " + str(round(100*f1.mean(),2)) + "%")

# Predict output for a test datapoint
input_data = ['20', 'Private', '1998', 'Some-college', '16', 'Married-civ-spouse', 'Prof-specialty', 
              'Husband', 'White', 'Male', '1000000', '0', '40', 'Poland']

# Encode test datapoint
input_data_encoded = [-1] * len(input_data)
count = 0
for i, item in enumerate(input_data):
    if item.isdigit():
        input_data_encoded[i] = int(input_data[i])
    else:
        input_data_encoded[i] = int(label_encoder[count].transform([input_data[i]]))
        count += 1

input_data_encoded = np.array(input_data_encoded)

# Run classigier on encoded datapoint and print output
predicted_class = classifier.predict([input_data_encoded])
print(label_encoder[-1].inverse_transform(predicted_class[0]))






