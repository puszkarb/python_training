#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 17:01:46 2018

@author: puszek
"""

import datetime
import json

import numpy as np
import matplotlib.pyplot as plt
from sklearn import covariance, cluster
from mpl_finance import quotes_historical_yahoo_ochl as quotes_yahoo

# Input file contains company symbols
input_file = 'company_symbol_mapping.json'

# Load the company symbol map
with open(input_file, 'r') as f:
    company_symbols_map = json.loads(f.read())

symbols, names = np.array(list(company_symbols_map.items())).T

# Load the hystorical stock quotes
start_date = datetime.datetime(2003, 7, 3)
end_date = datetime.datetime(2007, 5, 4)
quotes = [quotes_yahoo(symbol, start_date, end_date, asobject = True) for symbol in symbols]

# Extract opening and closing quotes
opening_quotes = np.array([quote.open for quote in quotes]).astype(np.float)
closing_quotes = np.array([quote.close for quote in quotes]).astype(np.float)

# Compute differences between opening and closing quotes
quotes_diff = closing_quotes - opening_quotes

# Data normalisation
X = quotes_diff.copy().T
X /= X.std(axis = 0)

# Create graph model
edge_model = covariance.GraphLassoCV()

"""

It turned out that the matplotlib.finance import quotes_historical_yahoo_ochl is no longer valid
as the hole .finance part of matplot lib. It was therefore launched as 3rd part library - yet,
I have not found equivalent solutions to those given in the book. This project is now officialy
left to die - this script only ofc. 

"""