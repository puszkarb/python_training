#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 22:50:47 2018

@author: puszek
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import MeanShift, estimate_bandwidth
from itertools import cycle

# Load data from input textfile
X = np.loadtxt('data_clustering.txt', delimiter = ',')

# Estimate the bandwidth of X
bandwidth_x = estimate_bandwidth(X, quantile = 0.1, n_samples=len(X))

# Cluster data with meanshift
meanshift_model = MeanShift(bandwidth=bandwidth_x, bin_seeding = True)
meanshift_model.fit(X)

# Extract the center of clusters
clusters_centers = meanshift_model.cluster_centers_
print("\nCenters of Clusters:\n", clusters_centers)

# Estimate the number of clusters
labels = meanshift_model.labels_
num_clusters = len(np.unique(labels))
print("\nNumber of clusters in input data = ", num_clusters)

# Visualise the datapoints and cluster centers
plt.figure()
markers ='o*xvs'
for i, marker in zip(range(num_clusters), markers):
    #Plot points that belong to the current cluster
    plt.scatter(X[labels == i, 0], X[labels == i, 1], marker=marker, color='black')

# Plot the cluster center
cluster_center = meanshift_model.cluster_centers_
plt.plot(cluster_center[0], cluster_center[1], marker='o', markerfacecolor='black', markeredgecolor='black', markersize=15)
plt.title("Clusters")
plt.show()