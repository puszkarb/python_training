#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 23:17:34 2018

@author: puszek
"""

import csv

import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import MeanShift, estimate_bandwidth

# Load data from given file
input_file ='sales.csv'
file_reader = csv.reader(open(input_file, 'r' ), delimiter = ',' )

X = []
for count, row in enumerate(file_reader):
    if not count:
        names = row[ 1 : ]
        continue
    
    X.append([float(x) for x in row[ 1: ]])

# Conver X into a numpy array
X = np.array(X)

# Estimate the bandwidth
bandwidth = estimate_bandwidth(X, quantile = 0.8, n_samples = len(X))

# Compute clustering with MeanShift
meanshift_model = MeanShift(bandwidth = bandwidth, bin_seeding = True)
meanshift_model.fit(X)

labels = meanshift_model.labels_
cluster_centers = meanshift_model.cluster_centers_
num_clusters = len(np.unique(labels))

print('\nNumber of clusters = ', num_clusters )
print('\nCenters of those aformentioned clusters: ')
print('\t'.join([name[:3] for name in names]))
for cc in cluster_centers:
    print('\t'.join([str(int(x)) for x in cc]))

# Extract two features for visualisation puproses
cc_2d = cluster_centers[:, 1:3]

# Plot the cc
plt.figure()
plt.scatter(cc_2d[:, 0], cc_2d[:, 1], s = 120, edgecolors = 'black', facecolors = 'none')

offset = 0.25
plt.xlim(cc_2d[:, 0].min() - offset * cc_2d[:, 0].ptp(), cc_2d[:, 0].max() + offset * cc_2d[:, 0].ptp())
plt.ylim(cc_2d[:, 1].min() - offset * cc_2d[:, 1].ptp(), cc_2d[:, 1].max() + offset * cc_2d[:, 1].ptp())

plt.title(' Center of the cluster ')
plt.show()