#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 15:08:33 2018

@author: puszek
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patches

from sklearn import datasets
from sklearn.mixture import GMM
from sklearn.cross_validation import StratifiedKFold

# Load the iris dataset
iris = datasets.load_iris()

# Split dataset into training and testing subsets in 80/20 proportion
indices = StratifiedKFold(iris.target, n_folds=5)

# Take the first fold
train_index, test_index = next(iter(indices))

# Extract training data and labels
X_train = iris.data[train_index]
y_train = iris.target[train_index]

# Extract testing data and labels
X_test = iris.data[test_index]
y_test = iris.target[test_index]

# Extract the number of classes
num_classes = len(np.unique(y_train))

# Build GMM
classifier = GMM(n_components=num_classes, covariance_type = 'full', init_params='wc', n_iter = 20)

# Initialise th classifier means
classifier.means_ = np.array([X_train[y_train == i].mean(axis = 0) for i in range(num_classes)])

#train the classifier
classifier.fit(X_train)

# Draw boundaries
plt.figure()
colors = 'rgb'
for i, color in enumerate(colors):
    # Extract eigenvalues and eigenvectors
    eigenvalues, eigenvectors = np.linalg.eigh(classifier._get_covars()[i][:2, :2])
    
    #Normalise the eigen vector
    norm_vec = eigenvectors[0] / np.linalg.norm(eigenvectors[0])
    
    # Extract the angle of tilt
    angle = np.arctan2(norm_vec[1], norm_vec[0])
    angle = 180 * angle / np.pi
    
    # Scaling factor to magnif ythe ellipses
    # random value chose to suit needs
    scaling_factor = 8
    eigenvalues *= scaling_factor
    
    #Draw the ellipse
    ellipse = patches.Ellipse(classifier.means_[i, :2], eigenvalues[0], eigenvalues[1], 180 + angle, color = color)
    axis_handle = plt.subplot(1, 1, 1)
    ellipse.set_clip_box(axis_handle.bbox)
    ellipse.set_alpha(0.6)
    axis_handle.add_artist(ellipse)
    
# Plot the data
colors = 'rgb'
for i, colors in enumerate(colors):
    cur_data = iris.data[iris.target == i]
    plt.scatter(cur_data[:, 0], cur_data[:, 1], marker = 'o', facecolors='none', edgecolors = 'black', s = 40, label = iris.target_names[i])
    test_data = X_test[ y_test == i]
    plt.scatter(test_data[:, 0], test_data[:, 1], marker = 's', facecolors = 'black', edgecolors = 'black', s = 40, label = iris.target_names[i] )

# Compute predictions for training and testing dat
y_train_pred = classifier.predict(X_train)
accuracy_training = np.mean(y_train_pred.ravel() == y_train.ravel()) * 100
print('Accuracy on training data =', accuracy_training)

y_test_pred = classifier.predict(X_test)
accuracy_test = np.mean(y_test_pred.ravel() == y_test.ravel()) * 100
print('Accuracy on testing data =', accuracy_test)

plt.title('GMM classifier')

plt.show()
    
    
    