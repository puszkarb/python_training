#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 18:56:49 2018

@author: puszek
"""

from logpy import run, var, fact
import logpy.assoccumm as la

# Define mathematical operations
add = 'addition'
mul = 'multiplication'

# Declaring commutativeness of those opperation using the facts system
fact(la.commutative, mul)
fact(la.commutative, add)
fact(la.associative, mul)
fact(la.associative, add)

# Define exaplar variables
a, b, c = var('a'), var('b'), var('c')

expression_orig= 3 * (-2) + (1 + 2 * 3) * (-1)

"""
expression1 = ( 1 + 2 x a) x b + 3 x c
expression2 = c x 3 + b x (2 x a + 1)
expression3 = (((2 x a) x b) + b) + 3 x c
"""


# Generating expressions
expression_orig = (add, (mul, 3, -2),(mul,(add, 1, (mul, 2, 3)), -1))
expression1 = (add, (mul, (add, 1, (mul, 2, a)), b), (mul, 3, c))
expression2 = (add, (mul, c, 3), (mul, b, (add, (mul, 2, a), 1)))
expression3 = (add, (add, (mul, (mul, 2, a), b), b), (mul, 3, c))

#Expressions comparison
print(run(0, (a, b, c), la.eq_assoccomm(expression1, expression_orig)))
print(run(0, (a, b, c), la.eq_assoccomm(expression2, expression_orig)))
print(run(0, (a, b, c), la.eq_assoccomm(expression3, expression_orig)))
