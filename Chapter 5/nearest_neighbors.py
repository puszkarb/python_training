#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 20:00:29 2018

@author: puszek
"""

"""
Extracting the neares neighbours
"""
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors

# Defining sample 2d datapoints
X = np.array([[2.1, 1.3], [1.3, 3.2], [2.9, 2.5], [2.7, 5.4], [3.8, 0.9],  
        [7.3, 2.1], [4.2, 6.5], [3.8, 3.7], [2.5, 4.1], [3.4, 1.9], 
        [5.7, 3.5], [6.1, 4.3], [5.1, 2.2], [6.2, 1.1]])

# Defining number of nearest neighbours
k = 5

# Testing datapoint
test_datapoint = np.array([[4.3, 2.7]])

# Plot input data
plt.figure()
plt.scatter(X[:, 0], X[:, 1], s= 100, marker ='o', color = 'black')
plt.scatter(test_datapoint[:, 0], test_datapoint[:, 1], s = 100, marker ='o', color = 'blue')
plt.title("Input data")



# Bulidng K Nearest Neighbours model
knn_model = NearestNeighbors(n_neighbors = k, algorithm = 'ball_tree').fit(X)
distances, indices = knn_model.kneighbors(test_datapoint)

# Print the 'k' nearest neighbours

print("\n K nearest neighbours: ")
for rank, index in enumerate(indices[0][:k], start = 1):
    print(str(rank) + " ===> ", X[index])
    
# Visualise the nearest neighbours along wit hthe test datapoint
plt.figure()
plt.title("Nearest Neighbors")
plt.scatter(X[:, 0], X[:, 1], s= 100, marker ='o', color = 'black')
plt.scatter(test_datapoint[:, 0], test_datapoint[:, 1], s = 100, marker ='o', color = 'blue')
plt.scatter(X[indices][0][:][:, 0], X[indices][0][:][:, 1], marker = 'o', color = "red", s = 250)

plt.show()

