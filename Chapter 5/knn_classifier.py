#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 20:31:10 2018

@author: puszek
"""

"""
K nearest neighbours classifier - still can't stand this lack of 'u' in neighbors

"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from sklearn import neighbors, datasets

# Load data input 
input_file = 'data.txt'
data = np.loadtxt(input_file, delimiter = ',')
X, y = data[:, : -1], data[:, -1].astype(np.int)

# Plot input data
plt.figure()
plt.title("Input data")
marker_shapes = 'v*os'
mapper = [marker_shapes[i] for i in y]
for i in range(X.shape[0]):
    plt.scatter(X[i, 0], X[i, 1], marker = mapper[i], s = 75, edgecolors = 'black', facecolors = 'none')

# Number of nearest neighbours
num_neighbours = 12

# Step size for grid visualisation
step_size = 0.01

# Create a K Nearest Neighbours classifier model
classifier = neighbors.KNeighborsClassifier(num_neighbours, weights='distance')

# Train the K Nearest Neighbours model
classifier.fit(X, y)

# Create the mesh to plot the boundaries
X_min, X_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
X_values, y_values = np.meshgrid(np.arange(X_min, X_max, step_size), np.arange(y_min, y_max, step_size))

# Evaluate the classifier on the grid points
output = classifier.predict(np.c_[X_values.ravel(), y_values.ravel()])

# Visualisation
output = output.reshape(X_values.shape)
plt.figure()
plt.pcolormesh(X_values, y_values, output, cmap = cm.Paired)

# Overlay training data on top of the mesh
for i in range(X.shape[0]):
    plt.scatter(X[i, 0], X[i, 1], marker=mapper[i], s = 50, edgecolors = 'black', facecolors = 'none')

plt.xlim(X_values.min(), X_values.max())
plt.ylim(y_values.min(), y_values.max())
plt.title('K Nearest Neighbours classifier model boundaries')

# Test input datapoint
test_datapoint = [7.1, 2.6]
plt.figure()
plt.title('Test datapoint')
for i in range(X.shape[0]):
    plt.scatter(X[i, 0], X[i, 1], marker = mapper[i], s = 75, edgecolors = 'black', facecolors = 'none')
    
plt.scatter(test_datapoint[0], test_datapoint[1], marker = 'x', linewidth = 6, s = 200, facecolors = 'black')

# Extract the K Neares neighbours
_, indices = classifier.kneighbors([test_datapoint])
indices = indices.astype(np.int)[0]

for i in indices:
    plt.scatter(X[i,0], X[i, 1], marker = mapper[y[i]], linewidth = 3, s = 100, facecolors = 'blue')

# Overlay the test point
plt.scatter(test_datapoint[0], test_datapoint[1], marker = 'x', linewidth = 6, s = 200, facecolors = 'red')

for i in range(X.shape[0]):
    plt.scatter(X[i, 0], X[i, 1], marker = mapper[i], s = 75, edgecolors = 'black', facecolors = 'none')
    
print("Predicted output:", classifier.predict([test_datapoint])[0])

plt.show()