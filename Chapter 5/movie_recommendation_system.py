#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 11:08:03 2018

@author: puszek
"""

import argparse
import json
import numpy as np

from collaborative_filtering import find_similar_users
from scores import pearson_score

def build_arg_parser():
    parser = argparse.ArgumentParser(description = 'Find the movies recomended for given user')
    parser.add_argument('--user', dest = 'user', required = True, help = 'Users for whom we want to find recomendation')
    return parser

# Get movie recomendations for given user
def get_recommendations(dataset, input_user):
    if input_user not in dataset:
        raise TypeError('Cannot find: ' + input_user + ' in the dataset')
    
    overall_scores = {}
    similarity_scores = {}
    
    for user in [x for x in dataset if x != input_user]:
        similarity_score = pearson_score(input_user, user, dataset)
        
        if similarity_score <= 0:
            continue
        
        filtered_list = [x for x in dataset[user] if x not in dataset[input_user] or dataset[input_user][x] == 0]
    
        for item in filtered_list:
            overall_scores.update({item: dataset[user][item] * similarity_score})
            similarity_scores.update({item: similarity_score})
        
    if len(overall_scores) == 0:
        return['No recommendations possible']
    
    # Generate movie ranks by normalisation
    movie_scores = np.array([[score/similarity_scores[item], item] for item, score in overall_scores.items()])
    
    # sort in decreasing order
    movie_scores = movie_scores[np.argsort(movie_scores[:,0])[::-1]]
    
    #Extract the movie recommmendations
    movie_recomendations = [movie for _,movie in movie_scores]
    
    return movie_recomendations

if __name__ == '__main__':
    args = build_arg_parser().parse_args()
    user = args.user
    ratings_file = 'ratings.json'
    
    with open(ratings_file, 'r') as f:
        data = json.loads(f.read())
        print("\n Movie recommendations for " + user + " :")
        recommendations = get_recommendations(data, user)
        for i, movie in enumerate(recommendations):
            print(str(i + 1) + ") " + movie )
    
    

