#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 10:29:39 2018

@author: puszek
"""

"""
About to build my veryfirst pipeline, it's goal is to select the
top K feautres from a given input data and then classify them
using an ERF classifier.

"""

from sklearn.datasets import samples_generator
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.pipeline import Pipeline
from sklearn.ensemble import ExtraTreesClassifier

# Generate some data, it will be labled and will be a base of our training and testing
X, y = samples_generator.make_classification(n_samples = 150, n_features = 150, n_informative = 6, n_redundant = 0, random_state = 7)

# First block in the pipeline, K best selector.
k_best_selector = SelectKBest(f_regression, k = 7)

# Next block in the pipeline is the ERF classifier with 60 estimators and maximum deptf of 4
classifier = ExtraTreesClassifier(n_estimators = 60, max_depth = 4)

# Building the pipeline
processor_pipeline = Pipeline([('selector', k_best_selector), ('ERF', classifier)])

# Train the pipeline using pregenerated data
processor_pipeline.fit(X, y)

# Predict the output for the input data
output = processor_pipeline.predict(X)
print("\nPredicted output:\n", output)

# Print scores
print("\nScore:", processor_pipeline.score(X, y))

# Print the features chosen by the pipeline selector
status = processor_pipeline.named_steps['selector'].get_support()

# Extract and print indices of selected features
selected = [ i for i, x in enumerate(status) if x]
print("\nIndices of selected features: ", ', '.join([str(x) for x in selected]))
