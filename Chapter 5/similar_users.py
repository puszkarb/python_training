#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 10:09:55 2018

@author: puszek
"""

import argparse
import json
import numpy as np

from scores import pearson_score

def build_arg_parser():
    parser = argparse.ArgumentParser(description = 'Find users who are similar to the given user')
    parser.add_argument('--user', dest = 'user', required = True, help = 'Input user - we are looking for ones similar to him')
    
    return parser

# Find users in the dataset that are similar to the input user
def find_similar_users( user, dataset, num_users):
    if user not in dataset:
        raise TypeError('Cannot find: ' + user + ' in the dataset')
        
    # Pearson score between the users and input user
    scores = np.array([[x, pearson_score( user, x, dataset)] for x in dataset if x != user])
    
    # Sort the scores
    scores_sorted = np.argsort(scores[:, 1])[::-1]
    
    #Extract the top 'num_users' scores
    top_users = scores_sorted[ : num_users]
    
    return scores[top_users]

if __name__ == '__main__':
    args = build_arg_parser().parse_args()
    user = args.user
    
    rating_file = 'ratings.json'
    
    with open(rating_file, 'r') as f:
        data = json.loads(f.read())
    
    print('\n4 most similar users to ' + user + ' : \n')
    similar_users = find_similar_users ( user, data, 4)
    print('User\t\tSimilarity score')
    print('-'*41)
    for item in similar_users:
        print(item[0], '\t\t', round(float(item[1]), 2) )
    

    
    
    